<?php

error_reporting(0);
error_reporting(E_ALL & ~E_WARNING);
error_reporting(E_ERROR);
error_reporting(E_ALL);
ini_set('display_errors', 0);


    require_once   './vendor/autoload.php';

    use Testtask\Rexit\DB;
    use Testtask\Rexit\Application;

    const PER_PAGE = 30;
    $config = require('./config.php');

    $db = new DB($config);
    $app = new Application();

    
    $paramArray = [];
    $postParameters = [];
    $getParameters= [];

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {

        foreach($_POST as $key=>$value) {
        $postParameters[$key] = $value;
      }

    } else if ($_SERVER['REQUEST_METHOD'] ==='GET') {
      $query = parse_str($_SERVER['QUERY_STRING'], $getParameters); 
      
    }
    $parameters = array_merge($postParameters, $getParameters);
    $paramArray = $app->sanitize($parameters);

    // var_dump($parameters, $paramArray);


    $page = $parameters['page'] ?? 1;
    $perPage = $parameters['perPage'] ?? PER_PAGE;
    $offset = ($page - 1) * $perPage; // Calculate the offset

    if (!empty($parameters['category'])) {
      $selectedCategory = $parameters['category'];
    }

    if (!empty($parameters['gender'])) {
      $selectedGender = $parameters['gender'];
    }

    if (!empty($parameters['age'])) {
      $selectedAge = $parameters['gender'];
    }


    $whereclause = ' WHERE 1 = 1 ';
    foreach($paramArray as $key=>$value) {
      if ($key == 'age') {
        $ageParams = explode('-', $value);
        $whereclause .= ' AND TIMESTAMPDIFF(YEAR, dob, CURDATE()) BETWEEN ' . $ageParams[0] . ' and ' . $ageParams[1] .' ' ;
      } else
      $whereclause .=' AND ' .$key . ' = ' .  '\'' . $value . '\'';
    }

     $users = $db->query("SELECT users.*, TIMESTAMPDIFF(YEAR, dob, CURDATE()) AS age FROM users " . $whereclause . " LIMIT :limit OFFSET :offset", 
    [':limit' => PER_PAGE, ':offset'=>intval($offset)])->fetchAll();

     $count = $db->query("SELECT count(*) as total  FROM users " . $whereclause)->fetchColumn();
     $categories = $db->query("SELECT DISTINCT category from users")->fetchAll(PDO::FETCH_COLUMN);

     $response = [
      'users' => $users,
      'totalRecords' => $count,
      'currentPage' => $page,
      'recordsPerPage' => $perPage,
  ];
    $pagination = json_encode($response);

    $totalPages = ceil($count / $perPage);
    $currentPage = max(1, min($totalPages, $page));

    require 'views/index.view.php';
