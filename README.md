# Rexit Test Task

## Getting Started

1. Clone the main branch of the repository:

    ```bash
    git clone -b main https://gitlab.com/blava/rexit-test-task.git
    ```

2. Navigate to the project directory:

    ```bash
    cd rexit-test-task
    ```

3. Run the following Docker command to build and start the containers:

    ```bash
    docker-compose up -d --build
    ```

   This command will start both `rexit_php` and `rexit_mysql` containers.

4. To execute `dataLoad.php` and load data from `dataset.txt` into the database, access the `rexit_php` container:

    ```bash
    docker exec -it rexit_php bash
    ```

5. Once inside the container, run the data loading script:

    ```bash
    php dataLoad.php
    ```

6. The program should now be accessible at [http://localhost:8080](http://localhost:8080).

**Note:** Make sure Docker is installed and running on your machine before executing the commands.

