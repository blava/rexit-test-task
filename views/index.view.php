<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  <title>Rexit Test Task</title>
</head>
<body>
<div class="container">
<?php require 'views/partials/navbar.view.php'; ?> 

<form method="POST">
<div class="row">  
    <div class="col-md-6">
        <label for="categories" class="form-label">Categories</label>
        <select class="form-select" id="categories" name="category">
            <option></option>
            <?php foreach ($categories as $category) : ?>
                <option value="<?= $category; ?>" <?= ($category == $selectedCategory) ? 'selected' : ''; ?>>
                    <?= $category; ?>
                </option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="col-md-6">
      <label for="gender" class="form-label">Gender</label>
      <select class="form-select" id="gender" name="gender">
        <option ></option>
        <option value="female"  <?= ('female' == $selectedGender) ? 'selected' : ''; ?>>female</option>
        <option value="male"  <?= ('male' == $selectedGender) ? 'selected' : ''; ?> >male</option>
      </select>
    </div>
</div>
<div class="row">  
    <div class="col-md-6">
    <label for="age" class="form-label">Age</label>
    <select class="form-select" id="age" name="age">
      <option></option>
      <option value="18-25"  <?= ('18-25' == $selectedAge) ? 'selected' : ''; ?>>18 - 25 </option>
      <option value="25-40"  <?= ('25-40' == $selectedAge) ? 'selected' : ''; ?>>25 - 40 </option>
      <option value="41-60"  <?= ('41-60' == $selectedAge) ? 'selected' : ''; ?>>41 - 50 </option>
      <option value="51-60"  <?= ('51-60' == $selectedAge) ? 'selected' : ''; ?>>51 - 60 </option>
    </select>
    </div>

    <div class="col-md-6 d-flex align-items-end justify-content-end">
        <button type="submit" class="btn btn-secondary mt-2">Search</button>
    </div>
</div>
</form>  


<div class="row mt-5">
        <div class="col-md-6">
            <h2 class="content-center">Users</h2>
        </div>
        <div class="col-md-6 text-end">
            <h5>Total Records (<?= $count; ?>)</h5>
        </div>
<table class="table">
<thead>
  <tr>
  <th scope="col">#</th>
      <th scope="col">Category</th>
      <th scope="col">First Name</th>
      <th scope="col">Last Name</th>
      <th scope="col">Email</th>
      <th scope="col">Gender</th>
      <th scope="col">Date of Birth</th>
      <th scope="col">Age</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach ($users as $user) : ?>
    <tr>  
       <?php foreach ($user as $v) : ?>
          <td> <?= $v ?> </td>
        <?php endforeach; ?>
      </tr>
  <?php endforeach; ?>
      </tbody>
  </table>


<nav >
  <ul class="pagination justify-content-center">
    <li class="page-item <?php echo ($currentPage <= 1) ? 'disabled' : ''; ?>">
      <a class="page-link" href="?page=<?php echo $currentPage - 1; ?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>

    <?php for ($i = max(1, $currentPage -5); $i <= min($totalPages,$currentPage + 5); $i++) : ?>
      <li class="page-item <?php echo ($i == $currentPage) ? 'active' : ''; ?>">
        <a class="page-link" href="?page=<?php echo $i . '&' .  http_build_query($paramArray) ?>"><?php echo $i; ?></a>
      </li>
    <?php endfor; ?>

    <li class="page-item <?php echo ($currentPage >= $totalPages) ? 'disabled' : ''; ?>">
      <a class="page-link" href="?page=<?php echo $currentPage + 1 ?>" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
    </div>

</div>
</body>
</html>


