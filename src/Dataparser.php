<?php

  namespace Testtask\Rexit;

  use Testtask\Rexit\DB;

  const BATCH_SIZE = 500; 

  class Dataparser {

    private $db;

    public function __construct(protected string $csvfile, DB $db) {

      $this->db = $db;
    }


    public function generateSQLdatafromCSV() {
      $row = 0; 
      $query = "INSERT INTO users(id, category, first_name, last_name, email, gender, dob) VALUES ";
      $queryDynamic = '';
      if (($handle = fopen($this->csvfile, "r")) !== FALSE ) {
      $title = fgetcsv($handle, 1024, ",");
      while ($data = fgetcsv($handle, 1024, ",")) {
        $sanitized = [];

        foreach($data as $datum) {
          if (strpos($datum ,'\'') > 0) {
            $datum = str_replace( '\'','', $datum);
          }
          $sanitized[] = $datum;
        }
        $sql = '(0, \'' .  implode('\',\'', $sanitized)  . '\'' . '),';
          
          $queryDynamic .= $sql;

          if (intval($row) % BATCH_SIZE === 0) {
            echo "Inserting in batches ..." . PHP_EOL;
            $q = $query . $queryDynamic;
            $q = trim($q, ',');
            $this->pushInDatabase($q);
            $queryDynamic ='';
          }
          $row++;
        }
        fclose($handle);  
        $query .=  $queryDynamic;
        $query = trim($query,',');
        $this->pushInDatabase($query);
      }
      }

    private function pushInDatabase(string $query) {
      // $query = $this->generateSQLQueryfromCSV();
     try {
       $this->db->query($query);
     } catch (PDOException $e) {
       echo $e->getMessage();
    }
    }  
    
    public static function getRootDir() :string {
      
      return dirname(__DIR__);
    }
    
  }
