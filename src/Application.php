<?php

namespace Testtask\Rexit;

class Application {
    private $sanitizedArray = ['category', 'first_name', 'last_name', 'gender', 'age'];

    public function sanitize($parameters) {
      $paramArray = [];
      foreach($parameters as $key=>$value) {
        if (in_array($key, $this->sanitizedArray) && !empty($value)) {
          $paramArray[$key] = $value;
        }
      }
      return $paramArray;
    }    

  }