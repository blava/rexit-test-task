<?php

  namespace Testtask\Rexit;

  use PDO;
  use PDOException;

  class DB {

    private PDO $pdo;

    public function __construct(array $config) {
        


      $dsn = "mysql:" .http_build_query($config, arg_separator:';');
      $user = 'root';
      $password = 'password';

        try {
          $this->pdo = new PDO($dsn, $user, $password, [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC]);

        } catch (PDOException $e) {
          echo "<h3>NOOOOOOOOO </h3>";
          echo $e->getMessage();
        }
    }


    public  function init() {
      try {
          // Create users table
          $sql = 'CREATE TABLE users (
                      id INT PRIMARY KEY AUTO_INCREMENT,
                      category VARCHAR(50), 
                      first_name VARCHAR(100),
                      last_name VARCHAR(255), 
                      email VARCHAR(100), 
                      gender varchar(20), 
                      dob DATE)';
          $this->pdo->exec($sql);
          print("Created users Table.\n");
      } catch (PDOException $e) {
          echo $e->getMessage();
      }
  }

  
  public function query(string $query, array $params = [])
  {
      $stmt = $this->pdo->prepare($query);
  
      foreach ($params as $param => $value) {
          if (is_int($value)) {
              $stmt->bindParam($param, intval($value), PDO::PARAM_INT);
          } else {
              $stmt->bindValue($param, $value);
          }
      }
  
       $stmt->execute();
       return $stmt;
       // return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
  

  public function paginate(string $query, array $params= []) {
    $stmt = $this->pdo->prepare($query);
  
    foreach ($params as $param => $value) {
        if (is_int($value)) {
            $stmt->bindParam($param, intval($value), PDO::PARAM_INT);
        } else {
            $stmt->bindValue($param, $value);
        }
    }
    $stmt->execute();
    $count =  count($stmt->fetchAll(PDO::FETCH_ASSOC));
    var_dump($count);
  }
  }