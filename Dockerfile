FROM php:8.2-apache

RUN apt update && apt install -y git curl wget libzip-dev
RUN docker-php-ext-install pdo pdo_mysql zip

# Set up Apache virtual host
COPY virtual_host.conf /etc/apache2/sites-available/000-default.conf
RUN a2enmod rewrite && a2enmod headers && a2ensite 000-default.conf

WORKDIR /var/www/html

COPY . /var/www/html

RUN wget https://getcomposer.org/composer-stable.phar -O /usr/local/bin/composer && chmod +x /usr/local/bin/composer


EXPOSE 8080

# CMD ["php", "/var/www/html/dataLoad.php"]

CMD ["apache2-foreground"]
