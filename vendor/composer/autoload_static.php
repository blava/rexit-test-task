<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitfde29b9306ae3b4d6faa61c6960ffd16
{
    public static $prefixLengthsPsr4 = array (
        'T' => 
        array (
            'Testtask\\Rexit\\' => 15,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Testtask\\Rexit\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitfde29b9306ae3b4d6faa61c6960ffd16::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitfde29b9306ae3b4d6faa61c6960ffd16::$prefixDirsPsr4;
            $loader->classMap = ComposerStaticInitfde29b9306ae3b4d6faa61c6960ffd16::$classMap;

        }, null, ClassLoader::class);
    }
}
