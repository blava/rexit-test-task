<?php

    require './vendor/autoload.php';

    use Testtask\Rexit\Dataparser;
    use Testtask\Rexit\DB;



    $config = require('config.php');

    $db = new DB($config);
    $db->init();


    $dp = new Dataparser('./dataset.txt', $db);
    echo $dp->generateSQLdatafromCSV() ; 
